#ifndef FLAME_OPERATIONS_ECHO_H
#define FLAME_OPERATIONS_ECHO_H

#include <cstdio>
#include <cstring>

void
echo_init(char* buffer, long value);

long
echo_parse(char* buffer);

void
echo_init_response(char* buffer, long value);

long
echo_parse_response(char* buffer);

#endif // FLAME_OPERATIONS_ECHO_H
